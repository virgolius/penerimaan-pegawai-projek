<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {

function __construct(){
  parent::__construct();
  $this->load->helper('url');
}

 public function index() {
   $data=array('title'=>'Tutorial Code Igniter - Java Web Media',
              'isi' =>'home/index_home');
              $this->load->view('layout/wrapper',$data);

 }

 public function kontak(){
   $data=array('title'=>'Tutorial Code Igniter - Java Web Media',
            'isi' => 'home/index_home');
            $this->load->view('kontak',$data);
   $this->load->view('kontak');
 }
  public function login(){
   $data=array('title'=>'Tutorial Code Igniter - Java Web Media',
            'isi' => 'home/index_home');
            $this->load->view('login',$data);
   $this->load->view('login');
 }
  public function daftar(){
   $data=array('title'=>'Tutorial Code Igniter - Java Web Media',
            'isi' => 'home/index_home');
            $this->load->view('daftar',$data);
   $this->load->view('daftar');
 }
}
